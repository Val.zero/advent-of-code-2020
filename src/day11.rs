use std::cmp::Ordering;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<Vec<u8>> {
    let input_path = Path::new("./assets/day11_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let mut res = Vec::new();
    let lines = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    for line in lines {
        let mut tmp = Vec::new();
        for c in line.chars() {
            tmp.push(char_to_u8(c));
        }
        res.push(tmp);
    }
    // TODO: optimise everything by using a single Vec<u8> instead of Vec<Vec<u8>>
    res
}

fn char_to_u8(c: char) -> u8 {
    match c {
        '.' => 0,
        'L' => 1,
        _ => 2,
    }
}

fn get_seat(x: usize, y: usize, seats: &Vec<Vec<u8>>) -> u8 {
    *seats.get(y).unwrap().get(x).unwrap()
}

// get a seat's neighbors
fn count_neighbors(x: usize, y: usize, seats: &Vec<Vec<u8>>) -> u8 {
    let mut res = 0;
    let xmax = seats.get(0).unwrap().len() - 1;
    let ymax = seats.len() - 1;
    if y != 0 {
        if x != 0 && get_seat(x - 1, y - 1, seats) == 2 {
            res += 1;
        }
        if get_seat(x, y - 1, seats) == 2 {
            res += 1;
        }
        if x != xmax && get_seat(x + 1, y - 1, seats) == 2 {
            res += 1;
        }
    }
    if x != 0 && get_seat(x - 1, y, seats) == 2 {
        res += 1;
    }
    if x != xmax && get_seat(x + 1, y, seats) == 2 {
        res += 1;
    }
    if y != ymax {
        if x != 0 && get_seat(x - 1, y + 1, seats) == 2 {
            res += 1;
        }
        if get_seat(x, y + 1, seats) == 2 {
            res += 1;
        }
        if x != xmax && get_seat(x + 1, y + 1, seats) == 2 {
            res += 1;
        }
    }
    res
}

// get a seat's next state
fn update_seat(x: usize, y: usize, seats: &Vec<Vec<u8>>) -> u8 {
    let seat = get_seat(x, y, seats);
    let mut res = seat;
    match seat {
        1 => {
            if count_neighbors(x, y, seats) == 0 {
                res = 2
            }
        }
        2 => {
            if count_neighbors(x, y, seats) > 3 {
                res = 1
            }
        }
        _ => {}
    }
    res
}

// change the whole room once
fn update_room(seats: &Vec<Vec<u8>>) -> (Vec<Vec<u8>>, usize) {
    let mut next_state = Vec::new();
    let mut diff = 0;
    let xmax = seats.get(0).unwrap().len();
    let ymax = seats.len();
    for y in 0..ymax {
        let mut upd_row = Vec::new();
        for x in 0..xmax {
            let seat = get_seat(x, y, seats);
            let next_seat = update_seat(x, y, seats);
            if seat != next_seat {
                diff += 1;
            }
            upd_row.push(next_seat);
        }
        next_state.push(upd_row);
    }
    (next_state, diff)
}

// find stable state
fn stable_state(seats: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let mut state = seats;
    let mut diff = 1;
    loop {
        if diff == 0 {
            break;
        }
        let (next_state, tmp_diff) = update_room(&state);
        state = next_state;
        diff = tmp_diff;
    }
    state
}

// count occupied seats
fn count_occupied(seats: &Vec<Vec<u8>>) -> usize {
    let mut res = 0;
    let xmax = seats.get(0).unwrap().len();
    let ymax = seats.len();
    for x in 0..xmax {
        for y in 0..ymax {
            match get_seat(x, y, seats) {
                2 => {
                    res += 1;
                }
                _ => {}
            }
        }
    }
    res
}

fn find_visible(
    x: usize,
    y: usize,
    seats: &Vec<Vec<u8>>,
    offset_x: isize,
    offset_y: isize,
) -> usize {
    let xmax = (seats.get(0).unwrap().len() - 1) as isize;
    let ymax = (seats.len() - 1) as isize;
    let newx = x as isize + offset_x;
    let newy = y as isize + offset_y;
    let res = if newx < 0 || newx > xmax || newy < 0 || newy > ymax {
        0
    } else {
        match get_seat(newx as usize, newy as usize, seats) {
            1 => 1,
            2 => 2,
            _ => {
                let new_offset_x = match offset_x.cmp(&0) {
                    Ordering::Less => offset_x - 1,
                    Ordering::Equal => 0,
                    Ordering::Greater => offset_x + 1,
                };
                let new_offset_y = match offset_y.cmp(&0) {
                    Ordering::Less => offset_y - 1,
                    Ordering::Equal => 0,
                    Ordering::Greater => offset_y + 1,
                };
                find_visible(x, y, seats, new_offset_x, new_offset_y)
            }
        }
    };
    res
}

fn count_visible(x: usize, y: usize, seats: &Vec<Vec<u8>>) -> u8 {
    let mut res = 0;
    let xmax = seats.get(0).unwrap().len() - 1;
    let ymax = seats.len() - 1;
    if y != 0 {
        if x != 0 && find_visible(x, y, seats, -1, -1) == 2 {
            res += 1;
        }
        if find_visible(x, y, seats, 0, -1) == 2 {
            res += 1;
        }
        if x != xmax && find_visible(x, y, seats, 1, -1) == 2 {
            res += 1;
        }
    }
    if x != 0 && find_visible(x, y, seats, -1, 0) == 2 {
        res += 1;
    }
    if x != xmax && find_visible(x, y, seats, 1, 0) == 2 {
        res += 1;
    }
    if y != ymax {
        if x != 0 && find_visible(x, y, seats, -1, 1) == 2 {
            res += 1;
        }
        if find_visible(x, y, seats, 0, 1) == 2 {
            res += 1;
        }
        if x != xmax && find_visible(x, y, seats, 1, 1) == 2 {
            res += 1;
        }
    }
    res
}

fn update_seat2(x: usize, y: usize, seats: &Vec<Vec<u8>>) -> u8 {
    let seat = get_seat(x, y, seats);
    let mut res = seat;
    match seat {
        1 => {
            if count_visible(x, y, seats) == 0 {
                res = 2
            }
        }
        2 => {
            if count_visible(x, y, seats) > 4 {
                res = 1
            }
        }
        _ => {}
    }
    res
}

// change the whole room once
fn update_room2(seats: &Vec<Vec<u8>>) -> (Vec<Vec<u8>>, usize) {
    let mut next_state = Vec::new();
    let mut diff = 0;
    let xmax = seats.get(0).unwrap().len();
    let ymax = seats.len();
    for y in 0..ymax {
        let mut upd_row = Vec::new();
        for x in 0..xmax {
            let seat = get_seat(x, y, seats);
            let next_seat = update_seat2(x, y, seats);
            if seat != next_seat {
                diff += 1;
            }
            upd_row.push(next_seat);
        }
        next_state.push(upd_row);
    }
    (next_state, diff)
}

// find stable state
fn stable_state2(seats: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let mut state = seats;
    let mut diff = 1;
    loop {
        if diff == 0 {
            break;
        }
        let (next_state, tmp_diff) = update_room2(&state);
        state = next_state;
        diff = tmp_diff;
    }
    state
}

pub fn process1() -> usize {
    let init_state = parse_input();
    let final_state = stable_state(init_state);
    count_occupied(&final_state)
}

pub fn process2() -> usize {
    let init_state = parse_input();
    let final_state = stable_state2(init_state);
    count_occupied(&final_state)
}
