use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day7_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

// Make dictionary : color -> nbr
// Make struct : bag_rules (nbr, contains, contains_times, contained_in)

fn make_dictionary(lines: &Vec<String>) -> HashMap<String, usize> {
    let mut res = HashMap::new();
    let mut i = 0;
    for line in lines {
        let color = line.split("bags").next().unwrap().trim().to_string();
        res.insert(color, i);
        i += 1;
    }
    res
}

fn color_to_nbr(color: &str, dictionary: &HashMap<String, usize>) -> usize {
    *dictionary.get(color).unwrap()
}

struct Bag {
    contains: Vec<usize>,
    contains_times: Vec<usize>,
    contained_in: Vec<usize>,
}

impl Bag {
    fn new_container(contains: Vec<usize>, contains_times: Vec<usize>) -> Bag {
        Bag {
            contains,
            contains_times,
            contained_in: Vec::new(),
        }
    }

    fn new_contained(contained_in: Vec<usize>) -> Bag {
        Bag {
            contains: Vec::new(),
            contains_times: Vec::new(),
            contained_in,
        }
    }

    fn add_containing(&mut self, clr: Vec<usize>, mult: Vec<usize>) {
        self.contains = clr;
        self.contains_times = mult;
    }

    fn add_container(&mut self, clr: usize) {
        self.contained_in.push(clr);
    }
}

fn trim_color(string: &str) -> &str {
    string
        .trim()
        .trim_end_matches(".")
        .trim_end_matches("s")
        .trim_end_matches("bag")
        .trim()
}

fn process_rules(lines: Vec<String>, dictionary: &HashMap<String, usize>) -> HashMap<usize, Bag> {
    // We are building a map where keys are color numbers and values are Bag
    let mut map: HashMap<usize, Bag> = HashMap::new();
    let count_re = Regex::new(r"[ ]*(\d) ([a-z ]*)[,.]*").unwrap();

    for line in lines {
        // We extract the line's color's name
        let mut values = line.split("contain");
        let color = trim_color(values.next().unwrap());
        let color_nbr = color_to_nbr(color, dictionary);

        // We parse the containment rules
        let rules = values.next().unwrap().split(",");
        let mut contains = Vec::new();
        let mut contains_times = Vec::new();
        for rule in rules {
            if rule != " no other bags." {
                let caps = count_re.captures(rule).unwrap();
                // A rule is a multiplicity and a color name (that we turn into number)
                let multiplicity = &caps[1].parse::<usize>().unwrap();
                let clr_ref = color_to_nbr(trim_color(&caps[2]), dictionary);
                // We add the rule to the Vec we're going to use for the Bag
                contains.push(clr_ref);
                contains_times.push(*multiplicity);
                // We also add the current Bag's reference in the rule's Bag (or make it if it's not already present)
                if let Some(bag) = map.get_mut(&clr_ref) {
                    bag.add_container(color_nbr);
                } else {
                    map.insert(clr_ref, Bag::new_contained(vec![color_nbr]));
                }
            }
        }
        // Finally we add the rules to the current Bag if it was in the map, or make it if not
        if let Some(bag) = map.get_mut(&color_nbr) {
            bag.add_containing(contains, contains_times);
        } else {
            map.insert(color_nbr, Bag::new_container(contains, contains_times));
        }
    }
    map
}

fn containers_set(color: usize, map: HashMap<usize, Bag>) -> HashSet<usize> {
    let mut set = HashSet::new();
    set.insert(color);
    let mut prev_size = 0;
    while set.len() != prev_size {
        prev_size = set.len();
        let mut new_set = HashSet::new();
        for bag in set {
            new_set.insert(bag);
            for contained in &map.get(&bag).unwrap().contained_in {
                new_set.insert(*contained);
            }
        }
        set = new_set;
    }
    set.remove(&color);
    set
}

fn containing_number(color: usize, map: &HashMap<usize, Bag>) -> usize {
    let mut res = 1;
    let bag = map.get(&color).unwrap();
    let contains = bag.contains.to_vec();
    let contains_times = bag.contains_times.to_vec();
    let size = contains.len();
    let mut contains_iter = contains.iter();
    let mut contains_times_iter = contains_times.iter();
    let mut i = 0;
    while i != size {
        let contained_bag = contains_iter.next().unwrap();
        let multiplicity = contains_times_iter.next().unwrap();
        res += (multiplicity) * containing_number(*contained_bag, map);
        i += 1;
    }
    res
}

pub fn process1() -> usize {
    let values = parse_input();
    let dictionary = make_dictionary(&values);
    let rules = process_rules(values, &dictionary);
    containers_set(color_to_nbr("shiny gold", &dictionary), rules).len()
}

pub fn process2() -> usize {
    let values = parse_input();
    let dictionary = make_dictionary(&values);
    let mut rules = process_rules(values, &dictionary);
    containing_number(color_to_nbr("shiny gold", &dictionary), &mut rules) - 1
}
