use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<usize> {
    let input_path = Path::new("./assets/day9_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader
        .lines()
        .collect::<io::Result<Vec<String>>>()
        .unwrap()
        .iter()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();
    values
}

fn is_sum_from(n: usize, l: &[usize]) -> bool {
    let mut res = false;
    let size = l.len();
    for i in 0..size {
        let m = l.get(i).unwrap();
        let complement = n.checked_sub(*m).unwrap_or(0);
        if l.contains(&complement) {
            res = true;
            break;
        }
    }
    res
}

fn find_first_error(list: &[usize]) -> usize {
    let mut res = 0;
    let size = list.len();
    for i in 25..size {
        let n = *list.get(i).unwrap();
        if !is_sum_from(n, &list[(i - 25)..i]) {
            res = n;
            break;
        }
    }
    res
}

fn find_sum_subset(error: usize, list: &[usize]) -> (usize, usize) {
    let mut res = (0, 0);
    let size = list.len();
    'outer: for i in 0..size {
        for j in i..size {
            let sum = list[i..=j].iter().sum();
            if error == sum {
                let a = *list[i..=j].iter().min().unwrap();
                let b = *list[i..=j].iter().max().unwrap();
                res = (a, b);
                break 'outer;
            }
        }
    }
    res
}

pub fn process1() -> usize {
    let values = parse_input();
    find_first_error(&values)
}

pub fn process2() -> usize {
    let values = parse_input();
    let error = find_first_error(&values);
    let (x, y) = find_sum_subset(error, &values);
    x + y
}
