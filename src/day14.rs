use regex::Regex;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::ops::{BitAnd, Shr};
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day14_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

// struct instr -> new, from_list, exec
enum Instr {
    Mask(Vec<u8>),
    Mem(u64, u64),
}

impl Instr {
    fn new_mask(s: &str) -> Instr {
        let mut res = Vec::new();
        for c in s.chars() {
            res.push(match c {
                '0' => 0,
                '1' => 1,
                _ => 2,
            })
        }
        Instr::Mask(res)
    }

    fn new_mem(add: u64, val: u64) -> Instr {
        Instr::Mem(add, val)
    }

    fn from_list(input: Vec<String>) -> Vec<Instr> {
        let mut res = Vec::new();
        let mask_re = Regex::new(r"mask = ([01X]*)").unwrap();
        let mem_re = Regex::new(r"mem\[([\d]*)] = ([\d]*)").unwrap();
        for line in input {
            if mask_re.is_match(&line) {
                let caps = mask_re.captures(&line).unwrap();
                res.push(Instr::new_mask(&caps[1]));
            } else if mem_re.is_match(&line) {
                let caps = mem_re.captures(&line).unwrap();
                let add = caps[1].parse::<u64>().unwrap();
                let val = caps[2].parse::<u64>().unwrap();
                res.push(Instr::new_mem(add, val));
            }
        }
        res
    }
}

fn find_max_addr(list: &[Instr]) -> u64 {
    let mut res = 0;
    for instr in list {
        match instr {
            Instr::Mask(_) => {}
            Instr::Mem(add, _) => {
                if *add > res {
                    res = *add;
                }
            }
        }
    }
    res
}

fn u64_to_bin_vec(val: u64) -> Vec<u8> {
    let mut res = Vec::new();
    let mut v = val;
    while v > 0 as u64 {
        if v % 2 == 0 {
            res.push(0);
        } else {
            res.push(1)
        }
        v = v.shr(1);
    }
    res
}

fn bin_vec_to_u64(val: &[u8]) -> u64 {
    let mut res = 0;
    let mut v = val.iter().cloned().collect::<Vec<u8>>();
    v.reverse();
    let mut i = 0;
    for n in v {
        res += n as u64 * 2_u64.pow(i);
        i += 1;
    }
    res
}

fn apply_mask(val: u64, mask: &[u8]) -> u64 {
    let mut res = Vec::new();
    let v0 = u64_to_bin_vec(val);
    let mut v = v0.iter();
    let mut m0 = mask.iter().cloned().collect::<Vec<u8>>();
    m0.reverse();
    let mut m = m0.iter();
    for _ in 0..36 {
        let elem_v = *v.next().unwrap_or(&0);
        let elem_m = *m.next().unwrap_or(&0);
        let elem_res = match elem_m {
            0 => 0,
            1 => 1,
            _ => elem_v,
        };
        res.push(elem_res);
    }
    res.reverse();
    bin_vec_to_u64(&res)
}

fn exec_list(list: &[Instr]) -> Vec<u64> {
    let max_addr = find_max_addr(list);
    let mut mem_state = vec![0; (max_addr + 1) as usize];
    let mut cur_mask = Vec::new();
    for instr in list {
        match instr {
            Instr::Mask(v) => cur_mask = v.clone(),
            Instr::Mem(add, val) => {
                let new_val = apply_mask(*val, &cur_mask);
                let add = *add as usize;
                mem_state.splice(add..(add + 1), vec![new_val].iter().cloned());
            }
        }
    }
    mem_state
}

pub fn process1() -> u64 {
    let input = parse_input();
    let instructions = Instr::from_list(input);
    let final_mem_state = exec_list(&instructions);
    final_mem_state.iter().sum()
}
