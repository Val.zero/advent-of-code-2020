use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day2_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

struct PasswordValidation {
    min: usize,
    max: usize,
    c: char,
    word: String,
}

impl PasswordValidation {
    fn new(src: String) -> PasswordValidation {
        let src_pair: Vec<&str> = src.split(':').collect();
        assert_eq!(src_pair.len(), 2);
        let rule = src_pair[0];
        let word = src_pair[1].trim().to_owned();
        let rule_pair: Vec<&str> = rule.split(' ').collect();
        assert_eq!(rule_pair.len(), 2);
        let min_max = rule_pair[0];
        let c = rule_pair[1].chars().next().unwrap();
        let min_max_pair: Vec<&str> = min_max.split('-').collect();
        assert_eq!(min_max_pair.len(), 2);
        let min = min_max_pair[0].parse::<usize>().unwrap();
        let max = min_max_pair[1].parse::<usize>().unwrap();

        PasswordValidation { min, max, c, word }
    }

    fn is_valid1(&self) -> bool {
        let mut chars = self.word.chars();
        let mut res = false;
        let mut count = 0;
        while let Some(c) = chars.next() {
            if c == self.c {
                count += 1;
                if res {
                    // if res is true : we were between the bounds
                    if count > self.max {
                        res = false;
                        // we are already off limits, no need to look further
                        break;
                    }
                } else {
                    // if res is false : we were under min
                    if count >= self.min {
                        res = true;
                    }
                }
            }
        }
        res
    }

    fn is_valid2(&self) -> bool {
        let mut chars = self.word.chars();
        // We take the (min)th char
        let cond1 = chars.nth(self.min - 1) == Some(self.c);
        // We take the (max)th char, taking into account part of the iter has already been consumed
        // so the index isn't max-1
        let cond2 = chars.nth(self.max - self.min - 1) == Some(self.c);
        cond1 ^ cond2
    }
}

pub fn process1() -> usize {
    let values = parse_input();
    let passwords: Vec<PasswordValidation> = values
        .iter()
        .map(|v| PasswordValidation::new(v.to_owned()))
        .collect();
    let mut iter = passwords.iter();
    let mut res = 0;
    while let Some(p) = iter.next() {
        if p.is_valid1() {
            res += 1;
        }
    }
    res
}

pub fn process2() -> usize {
    let values = parse_input();
    let passwords: Vec<PasswordValidation> = values
        .iter()
        .map(|v| PasswordValidation::new(v.to_owned()))
        .collect();
    let mut iter = passwords.iter();
    let mut res = 0;
    while let Some(p) = iter.next() {
        if p.is_valid2() {
            res += 1;
        }
    }
    res
}
