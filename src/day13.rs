use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::ops::Div;
use std::path::Path;

fn parse_input() -> (usize, Vec<usize>) {
    let input_path = Path::new("./assets/day13_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let mut lines = reader.lines();
    let dep_time = lines.next().unwrap().unwrap().parse::<usize>().unwrap();
    let line2 = lines.next().unwrap().unwrap();
    let buses = line2.split(',');
    let mut valid_buses = Vec::new();
    for bus in buses {
        if bus != "x" {
            valid_buses.push(bus.parse::<usize>().unwrap())
        }
    }
    (dep_time, valid_buses)
}

fn next_departs(buses: &[usize], cur_time: usize) -> Vec<usize> {
    let mut res = Vec::new();
    for bus in buses {
        res.push(bus * ((cur_time / bus) + 1));
    }
    res
}

fn min_bus_dep(buses: &[usize], next_dep: &[usize]) -> (usize, usize) {
    assert_eq!(buses.len(), next_dep.len());
    let size = buses.len();
    let mut min_dep = next_dep.iter().next().unwrap();
    let mut min_bus = buses.iter().next().unwrap();
    let mut i = 1;
    while i < size {
        let dep = next_dep.get(i).unwrap();
        if dep < min_dep {
            min_dep = dep;
            min_bus = buses.get(i).unwrap();
        }
        i += 1;
    }
    (*min_bus, *min_dep)
}

fn parse_input2() -> Vec<(usize, usize)> {
    let input_path = Path::new("./assets/day13_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let mut lines = reader.lines();
    let _ = lines.next();
    let line2 = lines.next().unwrap().unwrap();
    let buses = line2.split(',');
    let mut valid_buses = Vec::new();
    let mut i = 0;
    for bus in buses {
        if bus != "x" {
            valid_buses.push((bus.parse::<usize>().unwrap(), i))
        }
        i += 1;
    }
    valid_buses
}

fn multi_filter(list: Vec<usize>, buses: &[(usize, usize)]) -> Vec<usize> {
    let mut list = list;
    for bus in buses {
        let id = bus.0;
        let offset = bus.1;
        list = list
            .iter()
            .filter(|&&n| ((n + offset) % id) == 0)
            .cloned()
            .collect();
    }
    list
}

fn find_timestamp(buses: &mut [(usize, usize)]) -> usize {
    buses.sort_unstable();
    buses.reverse();
    let tmp0 = buses.first().unwrap().0;
    let tmp1 = buses.first().unwrap().1;
    let mut res = 0;
    let mut begin = 100000000000000 / tmp0;
    loop {
        let mut values = (begin..(begin + 100000000)).collect::<Vec<usize>>();
        values = values.iter().map(|n| (n * tmp0) - tmp1).collect();
        let fst = *values.first().unwrap();
        let filter_res = multi_filter(values, &buses);
        println!(
            "beginning with {} (first : {}) : {:?}",
            begin, fst, filter_res
        );
        if filter_res.len() > 0 {
            res = *filter_res.first().unwrap();
            break;
        }
        begin += 100000000;
    }
    res
}

// fn find_timestamp(buses: &mut [(usize, usize)]) -> usize {
//     buses.sort_unstable();
//     buses.reverse();
//     let mut res = 0;
//     let tmp = 100000000000000 / buses.first().unwrap().0;
//     let mut i = tmp;
//     loop {
//         let tmp0 = buses.first().unwrap().0;
//         let tmp1 = buses.first().unwrap().1;
//         res = tmp0 * i - tmp1;
//         //println!("highest is {} (offset : {}), i is {}, res is {}", tmp0, tmp1, i, res);
//         let mut flag = true;
//         for (id, offset) in buses.iter() {
//             if (res + *offset) % *id != 0 {
//                 flag = false;
//                 break;
//             }
//         }
//         if flag {
//             break;
//         }
//         i += 1;
//     }
//     res
// }

pub fn process1() -> usize {
    let (cur_time, buses) = parse_input();
    let next_departs = next_departs(&buses, cur_time);
    let (min_bus, min_dep) = min_bus_dep(&buses, &next_departs);
    min_bus * (min_dep - cur_time)
}

pub fn process2() -> usize {
    let mut buses = parse_input2();
    find_timestamp(&mut buses)
}
