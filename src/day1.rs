use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn complement_finder(values: Vec<usize>) -> Option<(usize, usize)> {
    let mut res = None;
    let mut i = 1;
    // We scan the list
    for v in &values {
        // Each value has a unique complement to 2020, we look for it in the list
        let complement = 2020 - v;
        if values[i..].contains(&complement) {
            // If it is found, our scan is over
            res = Some((v.to_owned(), complement));
            break;
        }
        i += 1;
    }
    res
}

fn trio_finder(values: Vec<usize>) -> Option<(usize, usize, usize)> {
    let mut res = None;
    // We use a similar algorithm, but double loops aren't satisfying ... Optimize ?
    let mut i = 1;
    for v1 in &values {
        for v2 in &values[i..] {
            let v = v1 + v2;
            // We don't want to overflow
            if v < 2020 {
                let complement = 2020 - (v1 + v2);
                if values[(i + 1)..].contains(&complement) {
                    res = Some((v1.to_owned(), v2.to_owned(), complement))
                }
            }
        }
        i += 1;
    }
    res
}

fn parse_input() -> Vec<usize> {
    let input_path = Path::new("./assets/day1_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader
        .lines()
        .collect::<io::Result<Vec<String>>>()
        .unwrap()
        .iter()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();
    values
}

pub fn process1() -> usize {
    let values = parse_input();
    let (x, y) = complement_finder(values).unwrap();
    x * y
}

pub fn process2() -> usize {
    let values = parse_input();
    let (x, y, z) = trio_finder(values).unwrap();
    x * y * z
}
