use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day3_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

fn char_to_u8(c: char) -> u8 {
    match c {
        '#' => 0,
        '.' => 1,
        _ => panic!("Wrong input : {}", c),
    }
}

fn make_grid(values: Vec<String>) -> Vec<Vec<u8>> {
    let mut grid = Vec::new();
    for line in values {
        let mut line_vec = Vec::new();
        for c in line.chars() {
            line_vec.push(char_to_u8(c));
        }
        grid.push(line_vec);
    }
    grid
}

fn compute_path(grid: &Vec<Vec<u8>>, right: usize, down: usize) -> usize {
    let mut res = 0;
    let mut grid_iter = grid.iter();
    let mut line = grid_iter.next();
    let max_x = line.unwrap().len();
    let mut x = 0;
    let mut over = false;
    loop {
        x = (x + right) % max_x;
        for _i in 0..down {
            line = grid_iter.next();
            if let None = line {
                over = true;
                break;
            }
        }
        if over {
            break;
        }
        if line.unwrap().iter().nth(x) == Some(&0) {
            res += 1;
        }
    }
    res
}

fn compute_mutliple_paths(grid: &Vec<Vec<u8>>, directions: Vec<(usize, usize)>) -> usize {
    let mut res = 1;
    for (right, down) in directions {
        res *= compute_path(grid, right, down);
    }
    res
}

pub fn process1() -> usize {
    let values = parse_input();
    let grid = make_grid(values);
    compute_path(&grid, 3, 1)
}

pub fn process2() -> usize {
    let values = parse_input();
    let grid = make_grid(values);
    let directions = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)].to_vec();
    compute_mutliple_paths(&grid, directions)
}
