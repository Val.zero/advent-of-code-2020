use std::collections::HashSet;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day6_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

fn process_line1(set: &mut HashSet<char>, line: &String) {
    for answer in line.chars() {
        set.insert(answer);
    }
}

fn make_answers1(values: Vec<String>) -> Vec<HashSet<char>> {
    let mut iter = values.iter();
    let mut res = Vec::new();
    while let Some(line) = iter.next() {
        let mut group_answers = HashSet::new();
        process_line1(&mut group_answers, line);
        while let Some(line) = iter.next() {
            if line.is_empty() {
                break;
            }
            process_line1(&mut group_answers, line);
        }
        res.push(group_answers);
    }
    res
}

fn process_line2(group: &mut Vec<Vec<char>>, line: &String) {
    let mut answers = Vec::new();
    for answer in line.chars() {
        answers.push(answer);
    }
    group.push(answers);
}

fn intersect(group: Vec<Vec<char>>) -> Vec<char> {
    let mut iter = group.iter();
    let mut res = iter.next().unwrap().to_owned();
    for answers in iter {
        let mut tmp = Vec::new();
        for answer in answers {
            if res.contains(answer) {
                tmp.push(*answer);
            }
        }
        res = tmp;
    }
    res
}

fn make_answers2(values: Vec<String>) -> Vec<Vec<char>> {
    let mut iter = values.iter();
    let mut res = Vec::new();
    while let Some(line) = iter.next() {
        let mut group_answers = Vec::new();
        process_line2(&mut group_answers, line);
        while let Some(line) = iter.next() {
            if line.is_empty() {
                break;
            }
            process_line2(&mut group_answers, line);
        }
        let intersect = intersect(group_answers);
        res.push(intersect);
    }
    res
}

pub fn process1() -> usize {
    let values = parse_input();
    make_answers1(values).iter().map(|set| set.len()).sum()
}

pub fn process2() -> usize {
    let values = parse_input();
    make_answers2(values).iter().map(|set| set.len()).sum()
}
