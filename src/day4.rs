use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day4_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

fn process_line<'a, 'b>(map: &'a mut HashMap<&'b str, &'b str>, line: &'b String) {
    let params = line.split(' ').collect::<Vec<&str>>();
    for param in params {
        let pair = param.split(':').collect::<Vec<&str>>();
        assert_eq!(pair.len(), 2);
        map.insert(pair[0], pair[1]);
    }
}

fn make_passports(values: &Vec<String>) -> Vec<HashMap<&str, &str>> {
    let mut iter = values.iter();
    let mut res = Vec::new();
    while let Some(line) = iter.next() {
        let mut map = HashMap::new();
        process_line(&mut map, line);
        while let Some(line) = iter.next() {
            if line.is_empty() {
                break;
            }
            process_line(&mut map, line);
        }
        res.push(map);
    }
    res
}

fn validate_passport1(passport: &HashMap<&str, &str>) -> bool {
    passport.contains_key("byr")
        && passport.contains_key("iyr")
        && passport.contains_key("eyr")
        && passport.contains_key("hgt")
        && passport.contains_key("hcl")
        && passport.contains_key("ecl")
        && passport.contains_key("pid")
}

fn validate_batch1(batch: Vec<HashMap<&str, &str>>) -> usize {
    let mut res = 0;
    for passport in batch {
        if validate_passport1(&passport) {
            res += 1;
        }
    }
    res
}

fn validate_passport2(
    passport: &HashMap<&str, &str>,
    yrre: &Regex,
    hgtre: &Regex,
    hclre: &Regex,
    eclre: &Regex,
    pidre: &Regex,
) -> bool {
    // First we want to check if all required fields are present
    let mut res = validate_passport1(passport);
    if !res {
        return false;
    }

    // We test formatting and values of year fields
    res &= yrre.is_match(passport.get("byr").unwrap());
    let byr = passport.get("byr").unwrap().parse::<usize>().unwrap();
    res &= 1920 <= byr;
    res &= byr <= 2002;
    res &= yrre.is_match(passport.get("iyr").unwrap());
    let iyr = passport.get("iyr").unwrap().parse::<usize>().unwrap();
    res &= 2010 <= iyr;
    res &= iyr <= 2020;
    res &= yrre.is_match(passport.get("eyr").unwrap());
    let eyr = passport.get("eyr").unwrap().parse::<usize>().unwrap();
    res &= 2020 <= eyr;
    res &= eyr <= 2030;

    // We check the height
    if !hgtre.is_match(passport.get("hgt").unwrap()) {
        return false;
    }
    let hgtcapt = hgtre.captures(passport.get("hgt").unwrap()).unwrap();
    let hgtval = *&hgtcapt[1].parse::<usize>().unwrap();
    match &hgtcapt[2] {
        "cm" => {
            res &= 150 <= hgtval;
            res &= 193 >= hgtval;
        }
        "in" => {
            res &= 59 <= hgtval;
            res &= 76 >= hgtval;
        }
        _ => res = false, // Should not happen
    }

    // We check hair color, eye color and passport ID
    res &= hclre.is_match(passport.get("hcl").unwrap());
    res &= eclre.is_match(passport.get("ecl").unwrap());
    res &= pidre.is_match(passport.get("pid").unwrap());

    res
}

fn validate_batch2(batch: Vec<HashMap<&str, &str>>) -> usize {
    let mut res = 0;
    let yrre = Regex::new("[0-9]{4}").unwrap();
    let hgtre = Regex::new(r"([0-9]{2,3})(cm|in)").unwrap();
    let hclre = Regex::new("#([0-9a-f]{6})").unwrap();
    let eclre = Regex::new(r"\A(amb|blu|brn|gry|grn|hzl|oth)\z").unwrap();
    let pidre = Regex::new(r"\A[0-9]{9}\z").unwrap();

    for passport in batch {
        if validate_passport2(&passport, &yrre, &hgtre, &hclre, &eclre, &pidre) {
            res += 1;
        }
    }
    res
}

pub fn process1() -> usize {
    let values = parse_input();
    let batch = make_passports(&values);
    validate_batch1(batch)
}

pub fn process2() -> usize {
    let values = parse_input();
    let batch = make_passports(&values);
    validate_batch2(batch)
}
