use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day5_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

fn find_number(n: usize, m: usize, input: &str) -> usize {
    if n == m {
        n
    } else {
        let a = (n + m + 1) / 2;
        let c = input.chars().next().unwrap();
        if (c == 'F') || (c == 'L') {
            find_number(n, a - 1, &input[1..])
        } else {
            find_number(a, m, &input[1..])
        }
    }
}

fn seat_id(input: &str) -> usize {
    let row = find_number(0, 127, &input[..7]);
    let column = find_number(0, 7, &input[7..]);
    row * 8 + column
}

pub fn process1() -> usize {
    parse_input().iter().map(|s| seat_id(s)).max().unwrap()
}

pub fn process2() -> usize {
    let values: Vec<usize> = parse_input().iter().map(|s| seat_id(s)).collect();
    let min = *values.iter().min().unwrap() as u64;
    let max = *values.iter().max().unwrap() as u64;
    let mut res = 0;
    for i in min..=max {
        if !values.contains(&(i as usize)) {
            res = i as usize;
            break;
        }
    }
    res
}
