use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::ops::{BitAnd, Shr};
use std::path::Path;

fn parse_input() -> Vec<usize> {
    let input_path = Path::new("./assets/day15_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    let numbers = values
        .iter()
        .next()
        .unwrap()
        .split(',')
        .collect::<Vec<&str>>()
        .iter()
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    numbers
}

fn parse_input2() -> Vec<u32> {
    let input_path = Path::new("./assets/day15_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    let numbers = values
        .iter()
        .next()
        .unwrap()
        .split(',')
        .collect::<Vec<&str>>()
        .iter()
        .map(|s| s.parse::<u32>().unwrap())
        .collect::<Vec<u32>>();
    numbers
}

fn remember(n: usize, spoken: &[usize]) -> usize {
    let size = spoken.len();
    let mut i = 0;
    let mut last_index = i;
    let mut present = false;
    for s in spoken {
        if (i < (size - 1)) && (*s == n) {
            last_index = i;
            present |= true;
        }
        i += 1;
    }
    if present {
        (size - 1) - last_index
    } else {
        0
    }
}

fn remember2(n: u32, spoken: &[u32]) -> u32 {
    let size = spoken.len();
    let mut i = size - 1;
    let mut res = 0;
    while i > 0 {
        if spoken.get(i - 1) == Some(&n) {
            res = ((size - 1) - (i - 1)) as u32;
            break;
        }
        i -= 1;
    }
    res
}

fn speak(mut spoken: Vec<usize>, limit: usize) -> usize {
    let mut i = spoken.len() - 1;
    while i < limit - 1 {
        let last_val = *spoken.get(i).unwrap();
        let next_val = remember(last_val, &spoken);
        spoken.push(next_val);
        i += 1;
    }
    *spoken.get(limit - 1).unwrap()
}

fn speak2(mut spoken: Vec<u32>, limit: usize) -> u32 {
    let mut i = spoken.len() - 1;
    while i < limit - 1 {
        println!("i is {}", i);
        let last_val = *spoken.get(i).unwrap();
        let next_val = remember2(last_val, &spoken);
        spoken.push(next_val);
        i += 1;
    }
    *spoken.get(limit - 1).unwrap()
}

pub fn process1() -> usize {
    let values = parse_input();
    speak(values, 2020)
}

pub fn process2() -> usize {
    let values = parse_input2();
    speak2(values, 30000000) as usize
}
