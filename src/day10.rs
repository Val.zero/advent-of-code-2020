use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::usize::MAX;

fn parse_input() -> Vec<usize> {
    let input_path = Path::new("./assets/day10_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let mut values: Vec<usize> = reader
        .lines()
        .collect::<io::Result<Vec<String>>>()
        .unwrap()
        .iter()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();
    // Sorting will make things easier and quicker here.
    values.sort_unstable();
    values
}

fn find_next_adapter(adapter: usize, list: &[usize]) -> (usize, usize) {
    let mut potential_res = Vec::new();
    for n in list.iter() {
        if potential_res.len() == 3 {
            break;
        } else {
            let diff = n.checked_sub(adapter).unwrap_or(MAX);
            if (diff != MAX) && (diff > 3) {
                break;
            }
            if (*n != adapter) && (n.checked_sub(adapter).unwrap_or(MAX) <= 3) {
                potential_res.push(n);
            }
        }
    }
    let res = **potential_res.iter().min().unwrap();
    (res, res - adapter)
}

fn find_adapter_differences(list: Vec<usize>) -> (usize, usize) {
    let mut res_1 = 0;
    let mut res_3 = 0;
    let mut adapter = *list.iter().min().unwrap();
    // first difference : outlet/adapter
    if adapter == 1 {
        res_1 += 1;
    } else if adapter == 3 {
        res_3 += 1;
    }
    let mut i = 1;
    let size = list.len();
    while i < size {
        let (next, diff) = find_next_adapter(adapter, &list);
        match diff {
            1 => res_1 += 1,
            3 => res_3 += 1,
            _ => {}
        }
        adapter = next;
        i += 1;
    }
    // final difference : adapter/device
    res_3 += 1;
    (res_1, res_3)
}

fn make_values(input: Vec<usize>) -> Vec<usize> {
    let max = input.iter().max().unwrap() + 3;
    [vec![0], input, vec![max]].concat()
}

fn find_paths(list: &[usize], index: usize, memo: &mut Vec<usize>) -> usize {
    let mut res = 0;
    let size = list.len();
    if index == (size - 1) {
        res = 1;
    } else {
        let memoval = *memo.get(index).unwrap();
        if memoval != 0 {
            res = memoval;
        } else {
            let adapter = list.get(index).unwrap();
            let mut next = Vec::new();
            for i in (index + 1)..size {
                if next.len() == 3 {
                    break;
                }
                let diff = (list.get(i).unwrap()) - adapter;
                if diff > 3 {
                    break;
                } else {
                    next.push(i);
                }
            }
            for n in next {
                res += find_paths(list, n, memo);
            }
            memo.splice(index..(index + 1), vec![res].iter().cloned());
        }
    }
    res
}

pub fn process1() -> usize {
    let values = parse_input();
    let (x, y) = find_adapter_differences(values);
    x * y
}

pub fn process2() -> usize {
    let value = make_values(parse_input());
    find_paths(&value, 0, &mut vec![0; value.len()])
}
