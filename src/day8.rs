use regex::Regex;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day8_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

#[derive(Copy, Clone)]
enum Instr {
    Acc(isize),
    Jmp(isize),
    Nop(isize),
}

fn indentify_instr(s: String, re: &Regex) -> (String, isize) {
    assert!(re.is_match(&*s));
    let caps = re.captures(&*s).unwrap();
    let val = match &caps[2] {
        "-" => 0 - caps[3].parse::<isize>().unwrap(),
        _ => caps[3].parse::<isize>().unwrap(),
    };
    (caps[1].to_string(), val)
}

fn make_list_instr(input: Vec<String>) -> Vec<Instr> {
    let mut res = Vec::new();
    let re = Regex::new(r"(acc|jmp|nop) ([+-])(\d*)").unwrap();
    for s in input {
        let (name, val) = indentify_instr(s, &re);
        let instr = match &name[..] {
            "acc" => Instr::Acc(val),
            "jmp" => Instr::Jmp(val),
            _ => Instr::Nop(val),
        };
        res.push(instr);
    }
    res
}

fn find_loop(list: Vec<Instr>) -> isize {
    let mut res = 0;
    let mut pc = 0;
    let mut visited = Vec::new();
    loop {
        if visited.contains(&pc) {
            break;
        }
        visited.push(pc);
        match list.get(pc).unwrap() {
            Instr::Acc(n) => {
                res += n;
                pc += 1;
            }
            Instr::Jmp(n) => {
                pc = ((pc as isize) + n) as usize;
            }
            Instr::Nop(_) => {
                pc += 1;
            }
        }
    }
    res
}

fn get_with_change(index: usize, vec: &Vec<Instr>, to_change: usize) -> Instr {
    if index == to_change {
        match vec.get(index).unwrap() {
            Instr::Acc(n) => Instr::Acc(*n),
            Instr::Jmp(n) => Instr::Nop(*n),
            Instr::Nop(n) => Instr::Jmp(*n),
        }
    } else {
        vec.get(index).unwrap().clone()
    }
}

fn exec_with_change(list: &Vec<Instr>, to_change: usize) -> Option<isize> {
    let mut res = 0;
    let mut pc = 0;
    let size = list.len();
    let mut visited = Vec::new();
    while pc < size {
        if visited.contains(&pc) {
            return None;
        }
        visited.push(pc);
        match get_with_change(pc, &list, to_change) {
            Instr::Acc(n) => {
                res += n;
                pc += 1;
            }
            Instr::Jmp(n) => {
                pc = ((pc as isize) + n) as usize;
            }
            Instr::Nop(_) => {
                pc += 1;
            }
        }
    }
    Some(res)
}

fn find_right_exec(list: Vec<Instr>) -> isize {
    let res;
    let mut i = 0;
    loop {
        if let Some(n) = exec_with_change(&list, i) {
            res = n;
            break;
        }
        i += 1;
    }
    res
}

pub fn process1() -> isize {
    let values = parse_input();
    let list = make_list_instr(values);
    find_loop(list)
}

pub fn process2() -> isize {
    let values = parse_input();
    let list = make_list_instr(values);
    find_right_exec(list)
}
