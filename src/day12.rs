use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn parse_input() -> Vec<String> {
    let input_path = Path::new("./assets/day12_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let values = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    values
}

enum Direction {
    North,
    South,
    East,
    West,
}

enum Instr {
    N(isize),
    S(isize),
    E(isize),
    W(isize),
    L(isize),
    R(isize),
    F(isize),
}

impl Instr {
    fn make(s: &str) -> Instr {
        let id = s.chars().next().unwrap();
        let val = s[1..].parse::<isize>().unwrap();
        match id {
            'N' => Instr::N(val),
            'S' => Instr::S(val),
            'E' => Instr::E(val),
            'W' => Instr::W(val),
            'L' => Instr::L(val),
            'R' => Instr::R(val),
            _ => Instr::F(val),
        }
    }

    fn make_list(input: Vec<String>) -> Vec<Instr> {
        let mut res = Vec::new();
        for line in input {
            res.push(Instr::make(&line));
        }
        res
    }
}

struct Waypoint {
    x: isize,
    y: isize,
}

impl Waypoint {
    fn new() -> Waypoint {
        Waypoint { x: 10, y: 1 }
    }

    fn move_by(&mut self, dir: Direction, val: isize) {
        match dir {
            Direction::North => self.y += val,
            Direction::South => self.y -= val,
            Direction::East => self.x += val,
            Direction::West => self.x -= val,
        }
    }

    fn rotate_left(&mut self, val: isize) {
        match val {
            90 => {
                let tmp = self.x;
                self.x = -self.y;
                self.y = tmp;
            }
            180 => {
                self.x = -self.x;
                self.y = -self.y;
            }
            _ => self.rotate_right(90),
        }
    }

    fn rotate_right(&mut self, val: isize) {
        match val {
            90 => {
                let tmp = self.y;
                self.y = -self.x;
                self.x = tmp;
            }
            180 => {
                self.x = -self.x;
                self.y = -self.y;
            }
            _ => self.rotate_left(90),
        }
    }
}

struct Ferry {
    x: isize,
    y: isize,
    dir: Direction,
    wp: Waypoint,
}

impl Ferry {
    fn new() -> Ferry {
        Ferry {
            x: 0,
            y: 0,
            dir: Direction::East,
            wp: Waypoint::new(),
        }
    }

    fn apply_instr(&mut self, instr: Instr) {
        match instr {
            Instr::N(n) => self.y += n,
            Instr::S(n) => self.y -= n,
            Instr::E(n) => self.x += n,
            Instr::W(n) => self.x -= n,
            Instr::L(n) => self.turn_left(n),
            Instr::R(n) => self.turn_right(n),
            Instr::F(n) => self.forward(n),
        }
    }

    fn apply_instr2(&mut self, instr: Instr) {
        match instr {
            Instr::N(n) => self.wp.move_by(Direction::North, n),
            Instr::S(n) => self.wp.move_by(Direction::South, n),
            Instr::E(n) => self.wp.move_by(Direction::East, n),
            Instr::W(n) => self.wp.move_by(Direction::West, n),
            Instr::L(n) => self.wp.rotate_left(n),
            Instr::R(n) => self.wp.rotate_right(n),
            Instr::F(n) => self.forward2(n),
        }
    }

    fn turn_left(&mut self, n: isize) {
        match self.dir {
            Direction::North => match n {
                90 => self.dir = Direction::West,
                180 => self.dir = Direction::South,
                _ => self.dir = Direction::East,
            },
            Direction::South => match n {
                90 => self.dir = Direction::East,
                180 => self.dir = Direction::North,
                _ => self.dir = Direction::West,
            },
            Direction::East => match n {
                90 => self.dir = Direction::North,
                180 => self.dir = Direction::West,
                _ => self.dir = Direction::South,
            },
            Direction::West => match n {
                90 => self.dir = Direction::South,
                180 => self.dir = Direction::East,
                _ => self.dir = Direction::North,
            },
        }
    }

    fn turn_right(&mut self, n: isize) {
        match self.dir {
            Direction::North => match n {
                90 => self.dir = Direction::East,
                180 => self.dir = Direction::South,
                _ => self.dir = Direction::West,
            },
            Direction::South => match n {
                90 => self.dir = Direction::West,
                180 => self.dir = Direction::North,
                _ => self.dir = Direction::East,
            },
            Direction::East => match n {
                90 => self.dir = Direction::South,
                180 => self.dir = Direction::West,
                _ => self.dir = Direction::North,
            },
            Direction::West => match n {
                90 => self.dir = Direction::North,
                180 => self.dir = Direction::East,
                _ => self.dir = Direction::South,
            },
        }
    }

    fn forward(&mut self, n: isize) {
        match self.dir {
            Direction::North => self.y += n,
            Direction::South => self.y -= n,
            Direction::East => self.x += n,
            Direction::West => self.x -= n,
        }
    }

    fn forward2(&mut self, n: isize) {
        self.x += n * self.wp.x;
        self.y += n * self.wp.y;
    }
}

pub fn process1() -> isize {
    let values = parse_input();
    let instructions = Instr::make_list(values);
    let mut ferry = Ferry::new();
    for instr in instructions {
        ferry.apply_instr(instr);
    }
    ferry.x.abs() + ferry.y.abs()
}

pub fn process2() -> isize {
    let values = parse_input();
    let instructions = Instr::make_list(values);
    let mut ferry = Ferry::new();
    for instr in instructions {
        ferry.apply_instr2(instr);
    }
    ferry.x.abs() + ferry.y.abs()
}
