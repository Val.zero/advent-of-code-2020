use regex::Regex;
use std::collections::HashSet;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::ops::{BitAnd, Shr};
use std::path::Path;

// Returns : invalid values, nearby ticket values
fn parse_input() -> (Vec<usize>, Vec<Vec<usize>>) {
    let input_path = Path::new("./assets/day16_input");
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let mut lines = reader.lines();
    let mut values = Vec::new();
    let mut tickets = Vec::new();
    let rule_re = Regex::new(r"[a-z ]*: ([\d]*)-([\d]*) or ([\d]*)-([\d]*)").unwrap();

    // We make a vec of all forbidden values
    let mut line = lines.next().unwrap().unwrap();
    let mut set = HashSet::new();
    while rule_re.is_match(&*line) {
        let caps = rule_re.captures(&*line).unwrap();
        let min1 = caps[1].parse::<usize>().unwrap();
        let max1 = caps[2].parse::<usize>().unwrap();
        let min2 = caps[3].parse::<usize>().unwrap();
        let max2 = caps[4].parse::<usize>().unwrap();
        for n in min1..=max1 {
            set.insert(n);
        }
        for n in min2..=max2 {
            set.insert(n);
        }
        line = lines.next().unwrap().unwrap();
    }
    let min_set = *set.iter().min().unwrap();
    let max_set = *set.iter().max().unwrap();
    values.push(min_set - 1);
    for n in min_set..=max_set {
        if !set.contains(&n) {
            values.push(n);
        }
    }
    values.push(max_set + 1);

    // We skip the "your ticket:" line
    line = lines.next().unwrap().unwrap();
    // And skip our ticket
    line = lines.next().unwrap().unwrap();
    while line != "" {
        line = lines.next().unwrap().unwrap();
    }

    // We skip the "nearby ticket:" line
    line = lines.next().unwrap().unwrap();
    // And parse nearby tickets
    while let Some(tmp) = lines.next() {
        line = tmp.unwrap();
        let ticket = line
            .split(',')
            .map(|n| n.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();
        tickets.push(ticket);
    }

    (values, tickets)
}

fn get_error_rate(tickets: &[Vec<usize>], forbidden: &[usize]) -> usize {
    let mut errors = Vec::new();
    let min = forbidden.iter().min().unwrap();
    let max = forbidden.iter().max().unwrap();
    for ticket in tickets {
        for n in ticket {
            if n < min || n > max || forbidden.contains(n) {
                errors.push(*n);
            }
        }
    }
    errors.iter().sum()
}

pub fn process1() -> usize {
    let (forbidden, tickets) = parse_input();
    get_error_rate(&tickets, &forbidden)
}
